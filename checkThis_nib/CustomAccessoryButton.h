//
//  CustomAccessoryButton.h
//  checkThis_nib
//
//  Created by ManGoes Mobile on 10/2/12.
//  CREATED TO SUPPORT ADDING SOME PROPERTIES.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAccessoryButton : UIButton
{
    NSArray *responses;
}
@property(strong,nonatomic) NSArray* responses;

@end
